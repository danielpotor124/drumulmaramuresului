import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  View,
  PermissionsAndroid,
  BackHandler,
  Image,
} from 'react-native';
import {WebView} from 'react-native-webview';
import messaging, {firebase} from '@react-native-firebase/messaging';
import FadeInOut from 'react-native-fade-in-out';
import Geolocation from '@react-native-community/geolocation';
import notifee, {EventType} from '@notifee/react-native';
import LocationEnabler from 'react-native-location-enabler';

const config = require('./config.json');

const appurl = config.appUrl;
let url = appurl;

//BACKGROUND NOTIFICATIONS
messaging().setBackgroundMessageHandler(async remoteMessage => {
  url = remoteMessage.data.target;
});

//REQUEST LOCATION
const requestLocationPermission = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
    } else {
    }
  } catch (err) {}
};

export default function App() {
  async function getToken() {
    return await firebase.messaging().getToken();
  }

  var token = getToken();
  async function requestUserPermission() {
    try {
      await firebase.messaging().requestPermission();
      getToken();
    } catch (error) {
      alert('Permission rejected');
    }
  }

  requestUserPermission();

  //LOCATION ENABLE PROMPT & GET LOCATION
  const {
    PRIORITIES: {BALANCED_POWER_ACCURACY},
    addListener,
    checkSettings,
    requestResolutionSettings,
  } = LocationEnabler;

  const listener = config.gps ? addListener() : null;

  const LocationConfig = {
    priority: BALANCED_POWER_ACCURACY, // default BALANCED_POWER_ACCURACY
    alwaysShow: true, // default false
    needBle: false, // default false
  };

  {
    config.gps
      ? [
          checkSettings(LocationConfig),
          requestResolutionSettings(LocationConfig),
        ]
      : null;
  }

  useEffect(() => [requestLocationPermission()], []);

  const getLocation = () => {
    return new Promise(resolve => {
      Geolocation.watchPosition(
        position => {
          const {longitude, latitude} = position.coords;

          resolve({longitude, latitude});
        },
        () => {},
        {enableHighAccuracy: true},
      );
    });
  };

  async function onDisplayNotification(remoteMessage) {
    const channelId = await notifee.createChannel({
      id: 'default',
      name: 'Default Channel',
    });
    await notifee.displayNotification({
      title: remoteMessage.notification.title,
      body: remoteMessage.notification.body,
      data: remoteMessage.data,
      android: {
        channelId,
        smallIcon: 'ic_small_icon',
        color: 'red',
        show_in_foreground: true,
      },
    });
  }

  //DATA POLLING
  useEffect(() => {
    const polling = setInterval(async () => {
      const coordinates = await getLocation();
      fetch(config.API, {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          token: token._W,
          longitude: coordinates && coordinates.longitude,
          latitude: coordinates && coordinates.latitude,
        }),
      }).catch(error => {
        console.log(error);
      });
    }, config.polling);
    return () => {
      clearInterval(polling);
    };
  }, []);

  // LOADER
  function Loader() {
    const [loaderState, setLoaderState] = useState(true);
    const [loaderFade, setLoaderFade] = useState(true);

    useEffect(() => {
      const interval = setTimeout(() => {
        setLoaderState(false);
      }, config.loader_lenght);
      return () => {
        clearTimeout(interval);
      };
    }, []);

    useEffect(() => {
      const fadeinterval = setTimeout(() => {
        setLoaderFade(false);
      }, config.loader_lenght - 700);
      return () => {
        clearTimeout(fadeinterval);
      };
    }, []);

    return (
      <View
        style={
          loaderState
            ? {
                justifyContent: 'center',
                ...StyleSheet.absoluteFillObject,
                backgroundColor: '#fbfbfb',
                zIndex: 1,
                alignItems: 'center',
              }
            : null
        }>
        <FadeInOut visible={loaderFade} duration={700}>
          <View style={{display: loaderState ? 'flex' : 'none'}}>
            <Image
              source={require('./assets/loader.gif')}
              style={{height: 200, width: 350}}
            />
          </View>
        </FadeInOut>
      </View>
    );
  }

  //WEBVIEW
  function WebviewComponent() {
    const [target, setTarget] = useState(
      `${url}?token=${JSON.stringify(token)}`,
    );
    const [hidden, setHidden] = useState(true);

    //FOREGROUND NOTIFICATION
    useEffect(() => {
      messaging().onMessage(async remoteMessage => {
        onDisplayNotification(remoteMessage);
      });
      return notifee.onForegroundEvent(({type, detail}) => {
        switch (type) {
          case EventType.DISMISSED:
            break;
          case EventType.PRESS:
            setTarget(detail.notification.data.target);
            url = detail.notification.data.target;
            break;
        }
      });
    }, []);

    useEffect(() => {
      const hideInterval = setTimeout(() => {
        setHidden(false);
        setTarget(`${url}?token=${token._W}`);
      }, 4000);
      return () => clearTimeout(hideInterval);
    }, []);

    return (
      <View style={{flex: 1}}>
        {hidden ? null : (
          <WebView
            source={{uri: target}}
            style={{flex: 1}}
            startInLoadingState
            thirdPartyCookiesEnabled={true}
            sharedCookiesEnabled={true}
          />
        )}
      </View>
    );
  }

  {
    config.gps ? listener.remove() : null;
  }

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'white'}}>
      <Loader />
      <View style={{flex: 1}}>
        <WebviewComponent />
      </View>
    </SafeAreaView>
  );
}
